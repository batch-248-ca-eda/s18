// console.log("Hello Wolrd");

// Functions
	// Functions are mostly created for complicated tasks to run several lines of code in succession.
	// Functions are also used to prevent repeating line/blocks of codes that perform the same task/functionality.

	let nickname = "Tolits";

	function printInput(){
		// let nickname = prompt("Enter your nickname:");
		console.log("Hi, "+nickname);
	}

	// printInput();

	// Consider this function
					//parameter 
	function printName(name){
		console.log("My nickname is " + name);
	} 

	// argument
	printName("Tolits");
	printName("Lito");

	// You can directly pass data into the function.

	// [SECTION] Parameters and Arguments

	// Parameter
		// "name" is called a parameter.
		// A "parameter" acts as a named variable that only exists inside a function.

	// Arguments
		// "Tolits" the information provided directly into the function is called an argument.
		// Values passed when invoking the function. ex. funcionName(argument);

	function checkDivisibilityBy8(num){
		 let remainder = num % 8;//expect 0 if divisible by 8
		 console.log("The remainder of " + num + " divided by 8 is " +remainder);
		 let isDivisibleBy8 = remainder === 0;
		 console.log("Is " + num + " divisible by 8?");
		 console.log(isDivisibleBy8);
	}  

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(25);

	/*
		mini-acitivity
		create a function that can check the divisibility by 4
		1. 56
		2. 95


	*/


		function checkDivisibilityBy4(num2){
		 let remainder2 = num2 % 4;//expect 0 if divisible by 8
		 console.log("The remainder of " + num2 + " divided by 4 is " +remainder2);
		 let isDivisibleBy4 = remainder2 === 0;
		 console.log("Is " + num2 + " divisible by 4?");
		 console.log(isDivisibleBy4);
	}  

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);

// [SECTION] Function as Argument
	// Function parameters can also accept other function as arguments.
	// Some complex functions use other functions as arguments to perform more complicated result.


	function argumentFunction(){
		console.log("This functionwas passed as an argument before the message was printed.")
	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}
	//function used without a parentheis is normally associated with using a FUNCTION as an ARGUMENT to another function
	invokeFunction(argumentFunction);
	// this is for finding information about a function in the console using console.log()
	console.log(argumentFunction);

	// [SECTION] Using multiple parameters
	// Multiple "arguments" will correspond to the number of parameters declared in a function in succeeding order

	function createFullName(firstName,middleName,lastName,character){
		console.log(firstName + " " + middleName + " " +lastName + " is the " + character + ".");
	}

	createFullName("Cardo","J.","Dalisay","Probinsyano");
	
	createFullName("Cardo","J.","Dalisay");

	createFullName("Cardo","J.","Dalisay","Probinsyano","Hello");//wont display the "Hello" in the console log

	// Use variable as arguments

	let firstName = "Monkey";
	let middleName = "D.";
	let lastName = "Luffy";
	let character = "protagonist"

	createFullName(firstName, middleName, lastName, character);

	// the order of the argument is the same to the order of the parameters

	function printFullName(middleName,firstName,lastName){
		console.log(firstName + ' ' + middleName + " " + lastName);
	}

	printFullName("Carlo","J.","Caparas");//J. Carlo Caparas


	// Mini Activity Start


	function printFriends(friend1,friend2,friend3){
		console.log("My three friends are: " + friend1 + ", " + friend2 + ", " + friend3 + ".");
	}

	printFriends("John","Jean","Jack");

	// Mini Activity End


	// [SECTION] The Return Statement

	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

	function returnFullName(firstName,middleName,lastName){
		return firstName + ' ' + middleName + ' ' + lastName;
		console.log("This will not be printed"); //not printed
	}

	// whatever value is returned from the return "returnFullName" function is now stored in the "completeName Variable"
	let completeName = returnFullName("Karen","L.","Davila");

	console.log(completeName + " is my bestfriend.");
	console.log(completeName);

	console.log(returnFullName(firstName,middleName,lastName));//printed "Monkey D. Luffy" because of variable

	function returnAddress(city,country){
		let fullAddres = city + "," + country;
		return fullAddres
	}

	let myAddress = returnAddress("Cebu City","Cebu");
	console.log(myAddress);


	function printPlayerInfo(username,level,job){
		console.log("Username: " + username);
		console.log("Level: " + level);
		console.log("Job: " + job);

	};

	//when a function has only console.log() to display its result it will return undefined
	let user1 = printPlayerInfo("knight_blank",96,"Paladin");
	console.log(user1);//undifined


	// Mini Activity Start

	let num1;
	let num2;

	function multiplyNumbers(num1,num2){
		return num1 * num2;
	}

	let product = multiplyNumbers(2,2);
	console.log("The product of " + num1 + " and " + num2 + " is " + product);//The product of 2 and 2 is 4

	// Mini Activity End

	